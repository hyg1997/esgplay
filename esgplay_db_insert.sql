/* Datos de prueba */

insert into User values (
	null,
    'ivanrod',
    null,
    '1234',
    null,
    null,
    null,
    null,
    null
);

insert into Game values (
	null,
    'Juego 1'
);

insert into Play values (
	null,
    1,
    false,
    '2018-10-15'
);

insert into Play values (
	null,
    1,
    true,
    '2018-10-15'
);

insert into Play values (
	null,
    1,
    true,
    '2018-10-15'
);

insert into PlayDetails values (
	null,
    1,
    1,
    1,
    10,
    15,
    5,
    3,
    3,
    12.44
);

insert into PlayDetails values (
	null,
    1,
    2,
    1,
    2,
    7,
    1,
    4,
    4,
    5.24
);